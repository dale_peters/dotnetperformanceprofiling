﻿using System;
using System.Collections.Generic;

namespace HighMemoryUsage
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = new List<object>();
            for (int i = 0; i < 5_000_000; i++)
            {
                a.Add(new { a = "1234567890" });
            }
            Console.WriteLine("Done");
        }
    }
}
