﻿using System;
using System.Threading;
public class ServerClass
{
    const int MIN_ITERATIONS = int.MaxValue / 1000;
    const int MAX_ITERATIONS = MIN_ITERATIONS + 10000;

    long m_totalIterations = 0;
    readonly object m_totalItersLock = new object();
    // The method that will be called when the thread is started.
    public void DoWork()
    {
        Console.WriteLine(
            "ServerClass.InstanceMethod is running on another thread.");

        var x = GetNumber();
    }

    private int GetNumber()
    {
        var rand = new Random();
        var iters = rand.Next(MIN_ITERATIONS, MAX_ITERATIONS);
        var result = 0;
        lock (m_totalItersLock)
        {
            m_totalIterations += iters;
        }
        // we're just spinning here
        // and using Random to frustrate compiler optimizations
        for (var i = 0; i < iters; i++)
        {
            result = rand.Next();
        }
        return result;
    }
}

public class Simple
{
    public static void Main()
    {
        for (int i = 0; i < 200; i++)
        {
            CreateThreads();
        }
    }

    public static void CreateThreads()
    {
        ServerClass serverObject = new ServerClass();

        Thread InstanceCaller = new Thread(new ThreadStart(serverObject.DoWork));
        // Start the thread.
        InstanceCaller.Start();

        Console.WriteLine("The Main() thread calls this after "
            + "starting the new InstanceCaller thread.");

    }
}